package lru_test

import (
	"testing"

	"gitlab.com/olzemal/lru-go/pkg/lru"
)

func TestCache(t *testing.T) {
	cache := lru.New(3)
	cache.Set("hello", "world")
	got := cache.Get("hello")
	if got != "world" {
		t.Errorf("got: `%v`; want: `world`", got)
	}
}

func TestZeroLength(t *testing.T) {
	cache := lru.New(0)
	cache.Set("foo", "bar")
	got := cache.Get("foo")
	if got != "" {
		t.Errorf("got: `%v`; want: ``", got)
	}
}

func TestMaxSize(t *testing.T) {
	cache := lru.New(3)
	cache.Set("one", "1")
	cache.Set("two", "2")
	cache.Set("three", "3")
	// Cache: three -> two -> one (full)
	cache.Set("four", "4")
	// Cache: four -> three -> two (full)
	for _, test := range []struct {
		key  string
		want string
	}{
		{"four", "4"},
		{"three", "3"},
		{"two", "2"},
		{"one", ""},
	} {
		got := cache.Get(test.key)
		if got != test.want {
			t.Errorf("got: `%v`; want `%v`", got, test.want)
		}
	}
}

func TestOverwrite(t *testing.T) {
	cache := lru.New(2)
	cache.Set("one", "1")
	cache.Set("one", "uno")
	got := cache.Get("one")
	if got != "uno" {
		t.Errorf("got: `%v`; want: `uno`", got)
	}
}

func TestUpdateAccessOrderOnRead(t *testing.T) {
	cache := lru.New(3)

	cache.Set("one", "1")
	cache.Set("two", "2")
	cache.Set("three", "3")
	// Cache: three -> two -> one

	got := cache.Get("one")
	if got != "1" {
		t.Errorf("got: `%v`; want: `1`", got)
	}
	// Cache: one -> three -> two

	cache.Set("four", "4")
	// Cache: four -> one -> three

	for _, test := range []struct {
		key  string
		want string
	}{
		{"one", "1"},
		{"two", ""},
		{"three", "3"},
		{"four", "4"},
	} {
		got := cache.Get(test.key)
		if got != test.want {
			t.Errorf("got: `%v`; want `%v`", got, test.want)
		}
	}
}

func TestGeneric(t *testing.T) {
	cache := lru.New(8)
	cache.Set(1, 2)
	cache.Set("hello", "world")
	cache.Set(2, "hello")
}
