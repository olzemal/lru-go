package lru

import (
	"container/list"
)

type cacheElement struct {
	elem  *list.Element
	value interface{}
}

// Cache holds the key value pairs, max size and the order of read/write
// accesses
type Cache struct {
	cache       map[interface{}]*cacheElement
	maxSize     int
	accessOrder list.List
}

// New returns an empty Cache
func New(max int) Cache {
	return Cache{
		cache:       map[interface{}]*cacheElement{},
		maxSize:     max,
		accessOrder: list.List{},
	}
}

// Set creates or updates a key value pair in the Cache
func (l *Cache) Set(key, value interface{}) {
	cacheElem, present := l.cache[key]
	if !present {
		elem := l.accessOrder.PushFront(key)
		l.cache[key] = &cacheElement{
			elem:  elem,
			value: value,
		}
	} else {
		l.accessOrder.MoveToFront(cacheElem.elem)
		l.cache[key].value = value
	}

	if l.accessOrder.Len() > l.maxSize {
		lastElem := l.accessOrder.Back()
		k := lastElem.Value.(string)

		l.accessOrder.Remove(lastElem)
		delete(l.cache, k)
	}
}

// Get retrieves a value for a given key if it is cached, else return ""
func (l *Cache) Get(key interface{}) interface{} {
	if cacheElem, ok := l.cache[key]; ok {
		l.accessOrder.MoveToFront(cacheElem.elem)
		return cacheElem.value
	}
	return ""
}
